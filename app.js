var config = require('./config.json'); 

var http = require('http');
var ecstatic = require('ecstatic')(__dirname +'/'+config.static_dir);
var router = require('./routes');
var bodyParser = require('./lib/bodyparser');
var server = http.createServer(function(req,res){
  bodyParser(req,res,function(rq,rs){
    var m = router.match(rq.url);
    if(m){ 
      rq.params = m.params;
      m.fn(rq,rs,m.next);
    }
    else ecstatic(rq,rs);
  });
});
server.listen(config.port,function(){
  console.log('Server started on port ' + config.port);
});
